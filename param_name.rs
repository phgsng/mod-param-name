// SPDX-License-Identifier: GPL-2.0

/*! hi clippy! */

use kernel::prelude::*;
use kernel::{c_str, Module};

struct ParamName;

module! {
    type: ParamName,
    name: "param_name",
    author: "Philipp Gesang",
    license: "GPL",
    params: {
        MODPARAM_FOO_BAR : bool {
            command_line_name: "foobar",
            default: true,
            permissions: 0o644,
            description: "distinct const name",
        },
        MODPARAM_XYZZY : i32 {
            default: 1337,
            permissions: 0o644,
            description: "same const name",
        },
    },
}

impl Module for ParamName {
    fn init(_name: &'static CStr, module: &'static ThisModule) -> Result<Self> {
        let lock = module.kernel_param_lock();
        let foobar = 42;
        pr_info!(
            "Param value: {} vs {}\n",
            foobar,
            MODPARAM_FOO_BAR.read(&lock)
        );

        Ok(ParamName)
    }
}
